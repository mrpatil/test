<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    public $timestamps = false;
	protected $table = 'albums';
	protected $primaryKey = 'id';
	protected $visible = ['title', 'id', 'description','img','date','featured'];
}
