<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	public $timestamps = false;
	protected $table = 'users';
	protected $primaryKey = 'id';
	
	public function basic()
	{
		return [
			'id' 				=>	$this->id,
			'name' 				=>	$this->name,
			'phone' 			=>	$this->phone,
			'bio' 				=>	$this->bio,
			'profile_picture' 	=>	$this->profile_picture,
		];
	}
	public function profileWithAlbums()
	{
		$profile = [
			'id' 				=>	$this->id,
			'name' 				=>	$this->name,
			'phone' 			=>	$this->phone,
			'bio' 				=>	$this->bio,
			'profile_picture' 	=>	$this->profile_picture,
		];
		$profile['album'] = $this->hasMany('App\Models\Album')->get()->toArray();
		return $profile;
	}
	public function albums()
    {
        return $this->hasMany('App\Models\Album');
	}
	
}
