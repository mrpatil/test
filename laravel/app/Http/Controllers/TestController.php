<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common;
use Illuminate\Support\Facades\DB;
use Request;

class TestController extends Controller
{
	
	public function getProfile() 
	{
		$id 	= Request::get('id');
		$user = \App\Models\User::find($id);
		if (!is_null($user)) {
			$user = $user->profileWithAlbums();
			$response = ['message'=> 'success','data'=>$user];
		} else {
			$response = ['message'=> 'not_found'];
		}
		return Common::sendResponse($response);	
	}
}