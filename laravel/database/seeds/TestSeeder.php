<?php

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->truncate();
		DB::table('albums')->truncate();
		$json = json_decode(file_get_contents(SERVER_API_PATH.'landscapes.json'), true); 
		$id = DB::table('users')->insertGetId([
			'name' => $json['name'],
			'phone' => $json['phone'],
			'email' => $json['email'],
			'bio' => $json['bio'],
			'profile_picture' => $json['profile_picture']
		]);
		$albums = $json['album'];
		foreach ($albums as $entry) {
			$albumId = DB::table('albums')->insertGetId([
				'title' => $entry['title'],
				'user_id'=>$id,
				'description' => $entry['description'],
				'img' => $entry['img'],
				'date' => $entry['date'],
				'featured' => $entry['featured']
			]);
		}

    }
}
