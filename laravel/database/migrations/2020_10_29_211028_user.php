<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email',45)->unique();
            $table->string('phone',15)->nullable();
            $table->string('bio',2024)->nullable();
            $table->string('profile_picture')->nullable();
            $table->dateTime('timestamp_added')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('timestamp_updated')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
		});

		Schema::create('albums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
			$table->string('title');
            $table->string('description',2024)->nullable();
			$table->string('img',512)->nullable();
			$table->boolean('featured')->default(false);
			$table->date('date')->nullable();
            $table->dateTime('timestamp_added')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('timestamp_updated')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
