Used framework

FrontEnd: Ionic Angular Framework
BackEnd: Laravel Framework/MySQL


**Things to do**
1) git clone https://mrpatil@bitbucket.org/mrpatil/test.git
2) go to laravel
(make sure main projcet folder name is test other wise need to change the API_URL in application folder)
3) run: composer install
4) run: php artisan migrate and php artisan db:seed
5) go to application
6) run: npm install
7) run: ng serve from application folder
8) http://localhost:4200/home/1


1. landscapes.json: Use this sample json file to get the data you need to show in the user interface, such as the photographer's profile details along with an Array that contains the data for each of gallery images.
=====================================================================================
**I have created a seeder for this you can find this file in laravel/public folder**

2. img (folder): This folder contains the images for both the gallery and profile sections.
=========================================================================================
**This folder is also in laravel/public**

3. wireframe.jpg: This is just an idea of the final user interface, showing the photographer's profile and gallery grid. (You could also suggest your own user interface idea)

I - Front-End

You must fetch the data from the json file asynchronously (ajax) and display in the layout as it shows in the wireframe.
You must develop the styles for each of the elements of the user interface.
**Done**
**I added variable in the url so it can be loaded based on user id**

* Feel free to use any framework, library or resource that you consider helpful for your development process. Any additional UI or UX ideas out of the provided wireframe guidelines will count as a bonus.

II - Back-end

- Design a database based off the provided data structure (landscapes.json). 
**Done**
- Build a RESTful API in PHP that returns the same data and format of the provided data structure (landscapes.json).
**Done**
- Build all the necessary models/controllers that you consider needed based off the provided data structure. Example (Photographer, Gallery, Photo, etc).
**Done**

* Please make sure to use Laravel for building the API
**Done**

You must attach a document in which you describe the development process and highlight what you consider was easy or difficult. Also provide a brief description explaining which tools did you decide to use and why did you consider these important for on your development. 
**It's not a difficult assignment but to set up it takes time install complete laravel and then new project and the setup stuff**
**I am using ionic angular they really lots of inbuild component and it's easier to go mobile in the future as everyone is creating mobile apps** 

** Please create and publish a repository (Git / Bitbucket) and share the link so we could track the process of your test.
**Done**