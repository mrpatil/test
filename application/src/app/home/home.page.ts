import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../services/global.service';

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage {

	public profile = null
	public id = 0;
	constructor(public global: GlobalService, public route: ActivatedRoute) {
		// tslint:disable-next-line:radix
		this.id = parseInt(this.route.snapshot.paramMap.get('userId'));
		this.getProfile();
	}

	getProfile() {
		this.global.sendAjaxRequest('getProfile', {id: this.id}).subscribe(response => {
			if (response.message === 'success') {
				this.profile = response.data;
			}
		});
	}

}
