import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Observable, from } from 'rxjs';
import { map, filter, scan, tap, retryWhen, retry } from 'rxjs/operators';
import $ from 'jquery';

@Injectable({
	providedIn: 'root'
})
export class GlobalService {

	public httpHeaders = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
	public httpReqOptions = new RequestOptions({ headers: this.httpHeaders });
	public API_URL = 'http://localhost/test/laravel/public';
	constructor(
		private ionicHttp: HTTP,
		private http: Http) {

	}
	sendAjaxRequest(requestURL, requestParam = {}, redirectOnUnauthorized = true): Observable<any> {
		requestURL = this.getRequestUrl(requestURL);
		return this.http.post(requestURL, this.param(requestParam), this.httpReqOptions).pipe(retry(1),
			map(response => {
				const result = response.json();
				return result;
			}));
	}
	param(data) { return $.param(data); }
	getRequestUrl(requestURL: string) {
		return this.API_URL + '/' + requestURL;
	}
}